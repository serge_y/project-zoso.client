﻿using UnityEngine;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game;

namespace Karen90MmoFramework.Client.Data
{
	public class MmoItemData
	{
		public short ItemId { get; set; }
		public MmoItemType ItemType { get; set; }
		public short Level { get; set; }
		public Rarity Rarity { get; set; }
		
		public int BuyoutPrice { get; set; }
		public int SellPrice { get; set; }

		public int MaxStack { get; set; }
		public bool IsTradable { get; set; }
		public short UseSpellId { get; set; }
		public UseLimit UseLimit { get; set; }

		public string Name { get; set; }
		public string Description { get; set; }
		public short IconId { get; set; }

		Texture2D icon;
		GUIContent drawContent;

		/// <summary>
		/// Gets the draw content
		/// </summary>
		public GUIContent DrawContent
		{
			get
			{
				if (drawContent == null)
					this.drawContent = new GUIContent(Icon, string.Format("{0}#{1}", TooltipStyle.Item, this.ItemId));

				return this.drawContent;
			}
		}

		/// <summary>
		/// Gets the item icon.
		/// </summary>
		public Texture2D Icon
		{
			get
			{
				if (icon == null && IconId > 0)
					this.icon = GameResources.Instance.LoadIcon(IconId);

				return this.icon;
			}
		}
	}
}
