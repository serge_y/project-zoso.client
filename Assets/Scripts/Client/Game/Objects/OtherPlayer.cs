﻿using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client.Game.Objects
{
	public class OtherPlayer : Character
	{
		#region Constructors and Destructors

		public OtherPlayer(MmoWorld world, MmoGuid guid, IAvatar avatar)
			: base(world, guid, avatar)
		{
		}

		#endregion

		#region Property System Implementation

		/// <summary>
		/// Called when a property has been set in the <see cref="System.Collections.Hashtable"/>
		/// </summary>
		protected override sealed void OnPropertySet(PropertyCode propertiesCode, object data)
		{
			switch (propertiesCode)
			{
				case PropertyCode.Name:
					{
						this.SetName(data as string);
						this.CharInfo.ExtendedName = this.Name;
					}
					break;

				default:
					{
						base.OnPropertySet(propertiesCode, data);
					}
					break;
			}
		}

		/// <summary>
		/// Levels up the player
		/// </summary>
		public void LevelUp()
		{
			this.SetLevel((byte)(Level + 1));
		}

		#endregion
	}
}
