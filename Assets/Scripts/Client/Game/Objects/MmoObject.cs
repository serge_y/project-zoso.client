using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game.Systems;

namespace Karen90MmoFramework.Client.Game.Objects
{
	public abstract class MmoObject
	{
		#region Constants and Fields

		enum ObjectState { Uninitialized, Initialized, Destroyed };

		/// <summary>
		/// State of the <see cref="MmoObject"/>.
		/// </summary>
		private ObjectState objectState;

		private readonly MmoGuid guid;
		private readonly MmoWorld world;
		private readonly IAvatar avatar;
		private readonly Hashtable properties;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the guid
		/// </summary>
		public MmoGuid Guid
		{
			get
			{
				return this.guid;
			}
		}

		/// <summary>
		/// Gets the family id
		/// </summary>
		public short FamilyId
		{
			get
			{
				return guid.SubId;
			}
		}

		/// <summary>
		/// Gets the world
		/// </summary>
		public MmoWorld World
		{
			get
			{
				return this.world;
			}
		}

		/// <summary>
		/// Gets the center
		/// </summary>
		public Vector3 Center
		{
			get
			{
				return this.Avatar.Center;
			}
		}

		/// <summary>
		/// Gets the avatar
		/// </summary>
		protected IAvatar Avatar
		{
			get
			{
				return this.avatar;
			}
		}

		/// <summary>
		/// Gets the current position
		/// </summary>
		public Vector3 Position { get; private set; }

		/// <summary>
		/// Gets the current rotation
		/// </summary>
		public Vector3 Rotation { get; private set; }

		/// <summary>
		/// Gets the POI flags
		/// </summary>
		public InterestFlags Flags { get; protected set; }

		/// <summary>
		/// Gets the name
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Gets the propertiesSet revision
		/// </summary>
		public int Revision { get; protected set; }

		/// <summary>
		/// Gets the value indicating whether this <see cref="MmoObject"/> is interactable or not.
		/// </summary>
		public bool IsInteractable { get; private set; }

		#endregion

		#region Constructors and Destructors

		protected MmoObject(MmoWorld world, MmoGuid guid, IAvatar avatar)
		{
			this.world = world;
			this.guid = guid;
			this.properties = new Hashtable();

			avatar.SetOwner(this);
			this.avatar = avatar;
			this.SetInteractability(false);

			this.objectState = ObjectState.Uninitialized;
			this.Name = string.Empty;

			this.Flags = InterestFlags.None;
		}

		#endregion

		#region Game System Methods

		/// <summary>
		/// Initializes the <see cref="MmoObject"/>.
		/// </summary>
		public void Initialize()
		{
			if (objectState == ObjectState.Uninitialized)
			{
				this.OnInitialize();
				this.objectState = ObjectState.Initialized;
			}
		}

		/// <summary>
		/// Spawns the <see cref="MmoObject"/>.
		/// </summary>
		public void Spawn(Vector3 position, Vector3 rotation)
		{
			if (objectState == ObjectState.Initialized)
				this.OnSpawn(position, rotation);
		}

		/// <summary>
		/// Updates the <see cref="MmoObject"/>.
		/// </summary>
		public void Update()
		{
			if (objectState == ObjectState.Initialized)
				this.OnUpdate();
		}

		/// <summary>
		/// Destroys the <see cref="MmoObject"/>.
		/// </summary>
		public void Destroy()
		{
			if (objectState == ObjectState.Initialized)
			{
				this.Avatar.Destroy();
				this.OnDestroy();

				if (properties.Count > 0)
				{
					this.properties.Add("revision", this.Revision);
					this.world.ObjectDataStorage.RemoveCachedProperties(guid);
					this.world.ObjectDataStorage.AddCachedProperties(guid, properties);
				}

				this.objectState = ObjectState.Destroyed;
			}
		}

		/// <summary>
		/// Called when the <see cref="MmoObject"/> is initialized
		/// </summary>
		protected virtual void OnInitialize()
		{
		}

		/// <summary>
		/// Called when the <see cref="MmoObject"/> is sapwned
		/// </summary>
		protected virtual void OnSpawn(Vector3 position, Vector3 rotation)
		{
		}

		/// <summary>
		/// Called during update
		/// </summary>
		protected virtual void OnUpdate()
		{
		}

		/// <summary>
		/// Called when the <see cref="MmoObject"/> is destroyed
		/// </summary>
		protected virtual void OnDestroy()
		{
		}

		#endregion

		#region Public Methods

		///// <summary>
		///// Sets the avatar
		///// </summary>
		///// <param name="newAvatar"></param>
		//public void SetAvatar(IAvatar newAvatar)
		//{
		//	if (newAvatar != null)
		//		newAvatar.SetOwner(this);
		//	this.Avatar = newAvatar;
		//	this.SetInteractability(false);
		//}

		/// <summary>
		/// Called when the Avatar becomes visible
		/// </summary>
		public virtual void OnBecameVisible()
		{
		}

		/// <summary>
		/// Called when the Avatar becomes invisible
		/// </summary>
		public virtual void OnBecameInvisible()
		{
		}

		#endregion

		#region Object Identification

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is of type <see cref="ObjectType.Npc"/>.
		/// </summary>
		public bool IsNpc()
		{
			return this.guid.Type == (byte)ObjectType.Npc;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is of type <see cref="ObjectType.Gameobject"/>.
		/// </summary>
		public bool IsGameobject()
		{
			return this.guid.Type == (byte)ObjectType.Gameobject;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is of type <see cref="ObjectType.Player"/>.
		/// </summary>
		public bool IsPlayer()
		{
			return this.guid.Type == (byte)ObjectType.Player;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a merchant.
		/// </summary>
		public virtual bool IsMerchant()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a civilian.
		/// </summary>
		public virtual bool IsCivilian()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a guard.
		/// </summary>
		public virtual bool IsGuard()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a trainer.
		/// </summary>
		public virtual bool IsTrainer()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a plant.
		/// </summary>
		public virtual bool IsPlant()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a vein.
		/// </summary>
		public virtual bool IsVein()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is an item.
		/// </summary>
		public virtual bool IsItem()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a chest.
		/// </summary>
		public virtual bool IsChest()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a spell object.
		/// </summary>
		public virtual bool IsSpellObject()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a doorway.
		/// </summary>
		public virtual bool IsDoorway()
		{
			return false;
		}

		/// <summary>
		/// Returns if the <see cref="MmoObject"/> is a portal.
		/// </summary>
		public virtual bool IsPortal()
		{
			return false;
		}

		#endregion

		#region Property System Implementation

		/// <summary>
		/// Set multiple propertiesSet
		/// </summary>
		public void SetProperties(IDictionary propertiesSet, int revision)
		{
			if (propertiesSet == null)
				return;

			foreach (DictionaryEntry entry in propertiesSet)
				this.SetProperty((PropertyCode) entry.Key, entry.Value);

			this.Revision = revision;
		}

		/// <summary>
		/// Sets a single property
		/// </summary>
		public void SetProperty(PropertyCode propertiesCode, object data)
		{
			this.properties[propertiesCode] = data;
			this.OnPropertySet(propertiesCode, data);
		}

		/// <summary>
		/// Called when a property has been set or changed
		/// </summary>
		protected virtual void OnPropertySet(PropertyCode propertiesCode, object data)
		{
			switch (propertiesCode)
			{
				case PropertyCode.Name:
					this.SetName((string) data);
					break;
			}
		}

		/// <summary>
		/// Set interest flags
		/// </summary>
		/// <param name="flags"></param>
		public virtual void SetInterestFlags(InterestFlags flags)
		{
		}

		/// <summary>
		/// Unset interest flags
		/// </summary>
		/// <param name="flags"></param>
		public virtual void UnsetInterestFlags(InterestFlags flags)
		{
		}

		/// <summary>
		/// Sets the name of the <see cref="MmoObject"/>.
		/// </summary>
		/// <param name="name"></param>
		protected void SetName(string name)
		{
			if(string.IsNullOrEmpty(name))
				return;

			this.Name = name.CapFirstLetter();
			this.Avatar.AvatarName = this.Name;
		}

		/// <summary>
		/// Sets the quest pickup status
		/// </summary>
		public void SetQuestPickupStatus(QuestPickupStatus pickupStatus)
		{
			switch (pickupStatus)
			{
				case QuestPickupStatus.Inactive:
					{
						this.UnsetFlags(InterestFlags.Quest);
					}
					break;

				case QuestPickupStatus.Active:
				case QuestPickupStatus.InProgress:
				case QuestPickupStatus.TurnIn:
					{
						this.SetFlags(InterestFlags.Quest);
					}
					break;
			}

			this.Avatar.SetOverheadIcon(GetOverheadIconFromQuestPickupStatus(pickupStatus));
			this.Avatar.SetBlipIcon(GetBlipIconFromQuestPickupStatus(pickupStatus));
		}

		/// <summary>
		/// Sets the quest status from flags
		/// </summary>
		protected void SetQuestStatusFromFlags(InterestFlags flags)
		{
			if ((flags & InterestFlags.QuestInProgress) == InterestFlags.QuestInProgress)
			{
				this.SetQuestPickupStatus(QuestPickupStatus.InProgress);
			}

			if ((flags & InterestFlags.QuestActive) == InterestFlags.QuestActive)
			{
				this.SetQuestPickupStatus(QuestPickupStatus.Active);
			}

			if ((flags & InterestFlags.QuestTurnIn) == InterestFlags.QuestTurnIn)
			{
				this.SetQuestPickupStatus(QuestPickupStatus.TurnIn);
			}
		}

		/// <summary>
		/// Gets the <see cref="BlipIconType"/> for a <see cref="QuestPickupStatus"/>.
		/// </summary>
		protected BlipIconType GetBlipIconFromQuestPickupStatus(QuestPickupStatus status)
		{
			switch (status)
			{
				case QuestPickupStatus.TurnIn:
					return BlipIconType.QuestTurnIn;

				case QuestPickupStatus.InProgress:
					return BlipIconType.QuestInProgress;

				case QuestPickupStatus.Active:
					return BlipIconType.QuestActive;
			}

			return BlipIconType.None;
		}

		/// <summary>
		/// Gets the <see cref="OverheadIconType"/> for a <see cref="QuestPickupStatus"/>.
		/// </summary>
		protected OverheadIconType GetOverheadIconFromQuestPickupStatus(QuestPickupStatus status)
		{
			switch (status)
			{
				case QuestPickupStatus.TurnIn:
					return OverheadIconType.QuestTurnIn;

				case QuestPickupStatus.InProgress:
					return OverheadIconType.QuestInProgress;

				case QuestPickupStatus.Active:
					return OverheadIconType.QuestActive;
			}

			return OverheadIconType.None;
		}

		/// <summary>
		/// Tells whether a flag has been set or not
		/// </summary>
		public bool HasFlag(InterestFlags flag)
		{
			return (this.Flags & flag) == flag;
		}

		/// <summary>
		/// Set flags
		/// </summary>
		protected void SetFlags(InterestFlags flag)
		{
			this.Flags |= flag;
		}

		/// <summary>
		/// Unset flags
		/// </summary>
		protected void UnsetFlags(InterestFlags flag)
		{
			this.Flags &= ~flag;
		}

		#endregion

		#region Movement System Implementation

		/// <summary>
		/// Moves the <see cref="MmoObject"/> to the new position.
		/// </summary>
		public virtual void Move(Vector3 newPosition)
		{
			newPosition.y = this.world.GetHeight(newPosition);
			this.SetPosition(newPosition, true);
		}

		/// <summary>
		/// Rotates the <see cref="MmoObject"/> using eular rotation.
		/// </summary>
		public virtual void Rotate(Vector3 newRotation)
		{
			this.Rotation = newRotation;
		}

		/// <summary>
		/// Sets the position
		/// </summary>
		protected void SetPosition(Vector3 newPosition, bool setAvatar)
		{
			this.Position = newPosition;
			if (setAvatar)
				this.Avatar.SetPosition(newPosition);
		}

		/// <summary>
		/// Rotates the <see cref="MmoObject"/> using eular rotation.
		/// </summary>
		protected virtual void SetRotation(Vector3 newRotation, bool setAvatar)
		{
			this.Rotation = newRotation;
			if(setAvatar)
				this.Avatar.SetRotation(newRotation);
		}

		/// <summary>
		/// Sets the movement direction
		/// </summary>
		public abstract void SetDirection(MovementDirection value);

		/// <summary>
		/// Sets the movement speed
		/// </summary>
		public abstract void SetSpeed(byte value);

		/// <summary>
		/// Sets the movement state
		/// </summary>
		public abstract void SetMovementState(MovementState value);

		#endregion

		#region Interaction System Implementation

		/// <summary>
		/// Called when this <see cref="MmoObject"/> becomes the player'r focus.
		/// </summary>
		public void OnPlayerFocus()
		{
			this.Avatar.HandlePlayerFocus();
		}

		/// <summary>
		/// Called when this <see cref="MmoObject"/> is no longer the player's focus.
		/// </summary>
		public void OnPlayerUnfocus()
		{
			this.Avatar.HandlePlayerUnfocus();
		}

		/// <summary>
		/// Sets the interactability for this <see cref="MmoObject"/>.
		/// </summary>
		/// <param name="isInteractable"></param>
		protected void SetInteractability(bool isInteractable)
		{
			var oldValue = this.IsInteractable;
			this.IsInteractable = isInteractable;

			if (isInteractable) // now = interactable
			{
				this.Avatar.AllowInteraction();
				if (!oldValue) // before = not interactable
				{
					if (World.Player.CurrentInteractor == this)
						World.Player.CloseInteraction();

					if (World.Player.CurrentTarget == this)
						World.Player.CloseTarget();
				}
			}
			else // now = not interactable
			{
				this.Avatar.DenyInteraction();
			}
		}

		#endregion

		#region Loot System Implementation

		/// <summary>
		/// Tells whether this <see cref="MmoObject"/> has loot or not
		/// </summary>
		public virtual bool HaveLoot()
		{
			return false;
		}

		/// <summary>
		/// Gets the loot.
		/// </summary>
		public virtual Loot GetLoot()
		{
			return null;
		}

		/// <summary>
		/// Sets the loot.
		/// </summary>
		/// <param name="loot"></param>
		public virtual void SetLoot(Loot loot)
		{
		}

		#endregion
	};
}


