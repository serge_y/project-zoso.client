﻿using System;
using Karen90MmoFramework.Game;
using UnityEngine;

using Karen90MmoFramework.Client.Game;
using Karen90MmoFramework.Client.Game.Objects;
using Karen90MmoFramework.Hud;

public class GameobjectAvatar : Avatar
{
	#region Constants and Fields

	private Gameobject goOwner;

	private Transform center;
	private Transform lootEffectInstance;

	private bool isFading;
	private bool fadeIn = true;

	/// <summary>
	/// overhead indicator object
	/// </summary>
	private GameObject overheadIndicator;
	private bool rotateOverheadIndicator;

	#endregion

	#region Properties

	/// <summary>
	/// Gets the center
	/// </summary>
	public override Vector3 Center
	{
		get
		{
			return center.position;
		}
	}

	#endregion

	#region Avatar Methods Overrides

	/// <summary>
	/// Called on Awake
	/// </summary>
	protected override void OnAwake()
	{
		this.ObjectRenderer = this.GetComponentInChildren<Renderer>();
		this.center = this.transform.Find("center");

		var matColor = this.ObjectRenderer.material.color;
		matColor.a = 0;
		this.ObjectRenderer.material.color = matColor;

		this.isFading = true;
	}

	/// <summary>
	/// Called on Update
	/// </summary>
	protected override void OnUpdate()
	{
		if (ShowTooltip)
		{
			TooltipManager.ValidateTooltip();
		}

		if (isFading)
		{
			this.HandleFading();
		}
	}

	/// <summary>
	/// Called when destroyed
	/// </summary>
	protected override void OnAvatarDestroy()
	{
		this.isFading = true;
		this.fadeIn = false;
	}

	/// <summary>
	/// Called after setting the owner
	/// </summary>
	/// <param name="newOwner"></param>
	protected override void OnSetOwner(MmoObject newOwner)
	{
		var owner = newOwner as Gameobject;
		if (owner == null)
			throw new InvalidCastException();

		this.goOwner = owner;
		this.AvatarName = newOwner.Name; // this.name != this.Name; remember is GameObject.name
	}

	/// <summary>
	/// Destroys all created <see cref="GameObject"/>.
	/// </summary>
	protected override void DestroyGameObject()
	{
		if (ShowTooltip)
		{
			TooltipManager.CloseTooltip();
		}

		Destroy(this.gameObject);
	}

	/// <summary>
	/// Called on mouse over
	/// </summary>
	protected override void OnBeginHighlight()
	{
		if (!isFading)
		{
			HUD.SetCursor(GetInteractionCursor());
			ObjectHighligher.OnHover(this.ObjectRenderer);
		}

		if (ShowTooltip == false)
		{
			HUD.BeginNameplate(goOwner, HUD.GetNameplateColor(goOwner));
			this.ShowTooltip = true;
		}
	}

	/// <summary>
	/// Gets the cursor for a certain <see cref="MmoObject"/>.
	/// </summary>
	/// <returns></returns>
	CursorType GetInteractionCursor()
	{
		if (goOwner.HasFlag(InterestFlags.Loot))
			return CursorType.CURSOR_USE;

		if (goOwner.HasFlag(InterestFlags.Usable))
			return CursorType.CURSOR_USE;

		if (goOwner.HasFlag(InterestFlags.Gatherable))
			return CursorType.CURSOR_USE;

		if (goOwner.HasFlag(InterestFlags.Collectible))
			return CursorType.CURSOR_USE;

		return CursorType.CURSOR_DEFAULT;
	}

	/// <summary>
	/// Called on mouse leave
	/// </summary>
	protected override void OnEndHighlight()
	{
		HUD.SetCursor(CursorType.CURSOR_DEFAULT);

		if (!isFading)
		{
			ObjectHighligher.OnDehover(this.ObjectRenderer);
		}

		if (ShowTooltip)
		{
			HUD.EndNameplate();
			this.ShowTooltip = false;
		}
	}

	#endregion

	#region Local Methods

	private void HandleFading()
	{
		var matColor = this.ObjectRenderer.material.color;

		if (fadeIn)
		{
			matColor.a += Time.deltaTime / 2;

			if (matColor.a >= 1)
			{
				this.isFading = false;
			}
		}
		else
		{
			matColor.a -= Time.deltaTime / 2;

			if (matColor.a <= 0)
			{
				this.isFading = false;
				this.DestroyGameObject();
			}
		}

		this.ObjectRenderer.material.color = matColor;
	}

	#endregion
}
