﻿using System.Collections.Generic;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Data;
using Karen90MmoFramework.Client.Game.Systems;

namespace Karen90MmoFramework.Client.Game.Objects
{
	public class Npc : Character
	{
		#region Constants and Fields

		private readonly NpcType npcType;

		private Loot loot;
		private short introMessageId;

		private List<MmoItemData> inventory;
		private List<MenuItemStructure> dialogue;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the npc type
		/// </summary>
		public NpcType NpcType
		{
			get
			{
				return this.npcType;
			}
		}

		/// <summary>
		/// Gets the inventory
		/// </summary>
		public IList<MmoItemData> Inventory
		{
			get
			{
				return this.inventory;
			}
		}

		/// <summary>
		/// Gets the dialogue
		/// </summary>
		public IList<MenuItemStructure> Dialogue
		{
			get
			{
				return this.dialogue;
			}
		}

		/// <summary>
		/// Gets the introduction message id
		/// </summary>
		public short IntroMessageId
		{
			get
			{
				return this.introMessageId;
			}
		}

		#endregion

		#region Constructors and Destructors

		public Npc(MmoWorld world, MmoGuid guid, NpcType npcType, IAvatar avatar)
			: base(world, guid, avatar)
		{
			this.npcType = npcType;
		}

		#endregion

		#region Game System Methods

		/// <summary>
		/// Called when the <see cref="Npc"/> is initialized
		/// </summary>
		protected override void OnInitialize()
		{
			base.OnInitialize();
			switch (npcType)
			{
				case NpcType.Merchant:
					{
						this.Avatar.SetOverheadIcon(OverheadIconType.Merchant);
						this.Avatar.SetBlipIcon(BlipIconType.Merchant);
					}
					break;
			}
		}

		/// <summary>
		/// Called when the <see cref="Npc"/> becomes visible
		/// </summary>
		public override void OnBecameVisible()
		{
			if (Alignment > SocialAlignment.Good && !this.IsDead)
			{
				this.World.Player.AddVisibleEnemy(this);
			}
		}

		/// <summary>
		/// Called when the <see cref="Npc"/> becomes invisible
		/// </summary>
		public override void OnBecameInvisible()
		{
			if (Alignment > SocialAlignment.Good && this.World.IsAlive)
			{
				this.World.Player.RemoveVisibleEnemy(this);
			}
		}

		#endregion

		#region Object Identification

		/// <summary>
		/// Returns if the <see cref="Npc"/> is of type <see cref="Karen90MmoFramework.Game.NpcType.Merchant"/>.
		/// </summary>
		public sealed override bool IsMerchant()
		{
			return this.npcType == NpcType.Merchant;
		}

		/// <summary>
		/// Returns if the <see cref="Npc"/> is of type <see cref="Karen90MmoFramework.Game.NpcType.Civilian"/>.
		/// </summary>
		public sealed override bool IsCivilian()
		{
			return this.npcType == NpcType.Civilian;
		}

		/// <summary>
		/// Returns if the <see cref="Npc"/> is of type <see cref="Karen90MmoFramework.Game.NpcType.Guard"/>.
		/// </summary>
		public sealed override bool IsGuard()
		{
			return this.npcType == NpcType.Guard;
		}

		/// <summary>
		/// Returns if the <see cref="Npc"/> is of type <see cref="Karen90MmoFramework.Game.NpcType.Trainer"/>.
		/// </summary>
		public sealed override bool IsTrainer()
		{
			return this.npcType == NpcType.Trainer;
		}

		#endregion

		#region Property System Implementation

		/// <summary>
		/// Called when a property has been set in the <see cref="System.Collections.Hashtable"/>
		/// </summary>
		protected override void OnPropertySet(PropertyCode propertiesCode, object data)
		{
			switch (propertiesCode)
			{
				case PropertyCode.ConversationId:
					this.introMessageId = (short)data;
					break;

				default:
					base.OnPropertySet(propertiesCode, data);
					break;
			}
		}

		#endregion

		#region Interaction System Implementation

		/// <summary>
		/// Set interest flags
		/// </summary>
		/// <param name="flags"></param>
		public override void SetInterestFlags(InterestFlags flags)
		{
			// there is loot for the player
			if ((flags & InterestFlags.Loot) == InterestFlags.Loot)
			{
				if (!HasFlag(InterestFlags.Loot))
				{
					this.Avatar.CreateLootIndicator();
					this.SetFlags(InterestFlags.Loot);
				}
			}

			if ((flags & InterestFlags.Quest) == InterestFlags.Quest)
			{
				this.SetQuestStatusFromFlags(flags);
			}

			if ((flags & InterestFlags.Shopping) == InterestFlags.Shopping)
			{
				this.SetFlags(InterestFlags.Shopping);
			}

			if ((flags & InterestFlags.Conversation) == InterestFlags.Conversation)
			{
				this.SetFlags(InterestFlags.Conversation);
			}

			// making this interactable if any flags are set
			if (!IsInteractable && (Flags != InterestFlags.None))
			{
				this.SetInteractability(true);
			}
		}

		/// <summary>
		/// Unset interest flags
		/// </summary>
		/// <param name="flags"></param>
		public override void UnsetInterestFlags(InterestFlags flags)
		{
			// removing loot
			if ((flags & InterestFlags.Loot) == InterestFlags.Loot)
			{
				if (HasFlag(InterestFlags.Loot))
				{
					this.Avatar.ClearLootIndicator();
					this.UnsetFlags(InterestFlags.Loot);
				}
			}

			if ((flags & InterestFlags.Quest) == InterestFlags.Quest)
			{
				this.SetQuestPickupStatus(QuestPickupStatus.Inactive);
			}

			if ((flags & InterestFlags.Shopping) == InterestFlags.Shopping)
			{
				this.UnsetFlags(InterestFlags.Shopping);
			}

			if ((flags & InterestFlags.Conversation) == InterestFlags.Conversation)
			{
				this.UnsetFlags(InterestFlags.Conversation);
			}

			// making this non-interactable if all flags are unset
			if (IsInteractable && (Flags == InterestFlags.None))
			{
				this.SetInteractability(false);
			}
		}

		/// <summary>
		/// Sets the inventory
		/// </summary>
		public void SetInventory(short[] itemIds)
		{
			// TODO: Temporary
			if (itemIds == null)
				return;

			if (inventory == null)
				this.inventory = new List<MmoItemData>();
			this.inventory.Clear();

			foreach (var itemId in itemIds)
			{
				MmoItemData itemData;
				if (MmoItemDataStorage.Instance.TryGetMmoItem(itemId, out itemData))
					this.inventory.Add(itemData);
			}
		}

		/// <summary>
		/// Sets the dialogue
		/// </summary>
		public void SetDialogue(MenuItemStructure[] menuItems)
		{
			// TODO: Temporary
			if (menuItems == null)
				return;

			if (dialogue == null)
				this.dialogue = new List<MenuItemStructure>();
			this.dialogue.Clear();

			foreach (var menuItem in menuItems)
				this.dialogue.Add(menuItem);
		}

		#endregion

		#region Combat System Implementation

		/// <summary>
		/// Called when this <see cref="Npc"/> is killed
		/// </summary>
		protected override void OnDeath()
		{
			this.Avatar.AvatarName += " (Dead)";

			if (npcType == NpcType.Enemy)
			{
				this.World.Player.RemoveVisibleEnemy(this);
			}
		}

		#endregion

		#region Loot System Implementation

		/// <summary>
		/// Tells whether this <see cref="Gameobject"/> has loot or not
		/// </summary>
		public override bool HaveLoot()
		{
			return this.loot != null;
		}

		/// <summary>
		/// Gets the <see cref="Loot"/> if there is loot
		/// </summary>
		public override Loot GetLoot()
		{
			return this.loot;
		}

		/// <summary>
		/// Sets the loot
		/// </summary>
		public override void SetLoot(Loot newLoot)
		{
			if (newLoot == null)
			{
				this.loot = null;
				if (HasFlag(InterestFlags.Loot))
				{
					this.Avatar.ClearLootIndicator();
					this.UnsetFlags(InterestFlags.Loot);
				}
			}
			else
			{
				this.loot = newLoot;
				if (!HasFlag(InterestFlags.Loot))
				{
					this.Avatar.CreateLootIndicator();
					this.SetFlags(InterestFlags.Loot);
				}
			}
		}

		#endregion
	}
}