﻿using UnityEngine;

using System;
using System.Collections.Generic;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client.Game
{
	public class MmoWorld
	{
		#region Constants and Fields

		private readonly GameHandler game;
		private readonly WorldObjectCache worldObjectCache;
		private readonly MmoItemDataStorage itemDataStorage;
		private readonly MmoObjectDataStorage objectDataStorage;

		private MinimapSystem map;

		#endregion

		#region Properties

		/// <summary>
		/// Tells whether the world is being played or not
		/// </summary>
		public bool IsAlive { get; private set; }

		/// <summary>
		/// Gets the otherPlayer
		/// </summary>
		public Player Player { get; private set; }

		/// <summary>
		/// Gets the game handler
		/// </summary>
		public GameHandler Game
		{
			get
			{
				return this.game;
			}
		}

		/// <summary>
		/// Gets the item data storage
		/// </summary>
		public MmoItemDataStorage ItemDataStorage
		{
			get
			{
				return this.itemDataStorage;
			}
		}

		/// <summary>
		/// MmoObject Cache. Contains the Properties for all Npcs, Items, and recently loaded players
		/// </summary>
		public MmoObjectDataStorage ObjectDataStorage
		{
			get
			{
				return this.objectDataStorage;
			}
		}

		/// <summary>
		/// Gets the map
		/// </summary>
		public IMinimap Map
		{
			get
			{
				return this.map;
			}
		}

		/// <summary>
		/// Called when the player is created
		/// </summary>
		public event Action<Player> OnPlayerAdded;
		
		/// <summary>
		/// Called between update cycles
		/// </summary>
		public event Action OnUpdate = () => { }; 

		#endregion

		#region Constructors and Destructors

		/// <summary>
		/// Creates a new instance of the <see cref="MmoWorld"/> class.
		/// </summary>
		/// <param name="gameHandler"></param>
		public MmoWorld(GameHandler gameHandler)
		{
			this.game = gameHandler;

			this.itemDataStorage = MmoItemDataStorage.Instance;
			this.objectDataStorage = MmoObjectDataStorage.Instance;
			this.worldObjectCache = new WorldObjectCache();
			
			this.itemDataStorage.Load();
			this.objectDataStorage.Load();
		}

		#endregion

		#region Game Methods

		/// <summary>
		/// Loads the world
		/// </summary>
		public void Load()
		{
		}

		/// <summary>
		/// Updates the world
		/// </summary>
		public void Update()
		{
			this.OnUpdate();
		}

		/// <summary>
		/// Unloads the world
		/// </summary>
		public void Unload()
		{
			this.IsAlive = false;
			this.worldObjectCache.Clear();

			this.OnUpdate = () => { };
			
			if (Player != null)
			{
				this.Player.Destroy();
				this.Player = null;
			}

			if (map != null)
			{
				this.map.Unload();
				this.map = null;
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Gets the height at the position
		/// </summary>
		public float GetHeight(Vector3 position)
		{
			if (Terrain.activeTerrain == null)
				return 0;

			return Terrain.activeTerrain.SampleHeight(position) + Terrain.activeTerrain.transform.position.y;
		}

		/// <summary>
		/// Gets the height at x, z coord
		/// </summary>
		public float GetHeight(float x, float z)
		{
			if (Terrain.activeTerrain == null)
				return 0;

			return Terrain.activeTerrain.SampleHeight(new Vector3(x, 0, z)) + Terrain.activeTerrain.transform.position.y;
		}

		/// <summary>
		/// Gets the updated position with the right hight according to the terrain
		/// </summary>
		public Vector3 GetInterpolatedPosition(Vector3 coord)
		{
			return new Vector3 {x = coord.x, y = this.GetHeight(coord), z = coord.z};
		}

		/// <summary>
		/// Adds a(n) <see cref="MmoObject"/> to the cache
		/// </summary>
		public bool AddMmoObject(MmoObject mmoObject)
		{
			if (this.worldObjectCache.AddItem(mmoObject))
			{
				this.OnUpdate += mmoObject.Update;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Retrieves a(n) <see cref="MmoObject"/>
		/// </summary>
		public bool TryGetMmoObject(MmoGuid guid, out MmoObject mmoObject)
		{
			return this.worldObjectCache.TryGetItem(guid, out mmoObject);
		}

		/// <summary>
		/// Removes a(n) <see cref="MmoObject"/>
		/// </summary>
		public bool RemoveMmoObject(MmoGuid guid)
		{
			MmoObject mmoObject;
			if (worldObjectCache.TryGetItem(guid, out mmoObject))
			{
				this.OnUpdate -= mmoObject.Update;
			}

			return worldObjectCache.RemoveItem(guid);
		}

		/// <summary>
		/// Removes all <see cref="MmoObject"/>s from the world
		/// </summary>
		public void RemoveAll()
		{
			this.worldObjectCache.ForEach(m => OnUpdate -= m.Update);
			this.worldObjectCache.Clear();
		}

		/// <summary>
		/// Sets the otherPlayer
		/// </summary>
		/// <param name="player"></param>
		public void SetPlayer(Player player)
		{
			if(Player != null)
				throw new InvalidOperationException("PlayerAlreadySet");

			this.Player = player;
			this.OnUpdate += Player.Update;

			this.map = GameObject.FindGameObjectWithTag("MapCam").AddComponent<MinimapSystem>();
			
			this.IsAlive = true;
			this.OnPlayerAdded(Player);
		}

		/// <summary>
		/// Updates the world of the player entry (usually after a world transfer or entered world)
		/// </summary>
		public void ValidateWorldForPlayerEntry()
		{
			this.map.Setup(Player);
		}

		/// <summary>
		/// Creates a(n) <see cref="CharacterAvatar"/>.
		/// </summary>
		public IAvatar CreatePlayerAvatar(MmoGuid id, int instanceId)
		{
			var instance = (GameObject) GameObject.Instantiate(game.Resources.LoadInstance(instanceId));
			instance.name = "P_" + id;
			instance.AddComponent<GamePersister>();
			return instance.AddComponent<Avatar>();
		}

		/// <summary>
		/// Create a(n) <see cref="CharacterAvatar"/>.
		/// </summary>
		public IAvatar CreateOtherPlayerAvatar(MmoGuid id, int instanceId)
		{
			var instance = (GameObject) GameObject.Instantiate(game.Resources.LoadInstance(instanceId));
			instance.name = "O_" + id;
			return instance.AddComponent<CharacterAvatar>();
		}

		/// <summary>
		/// Creates a(n) <see cref="CharacterAvatar"/>.
		/// </summary>
		public IAvatar CreateNpcAvatar(MmoGuid id, int instanceId)
		{
			var instance = (GameObject) GameObject.Instantiate(game.Resources.LoadInstance(instanceId));
			instance.name = "N_" + id;
			return instance.AddComponent<CharacterAvatar>();
		}

		/// <summary>
		/// Creates a(n) <see cref="GameobjectAvatar"/>.
		/// </summary>
		public IAvatar CreateGameObjectAvatar(MmoGuid id, int instanceId)
		{
			var instance = (GameObject) GameObject.Instantiate(game.Resources.LoadInstance(instanceId));
			instance.name = "G_" + id;
			return instance.AddComponent<GameobjectAvatar>();
		}
		
		/// <summary>
		/// Creates a(n) <see cref="DynamicAvatar"/>.
		/// </summary>
		public IAvatar CreateDynamicAvatar(MmoGuid id, int instanceId)
		{
			var instance = (GameObject) GameObject.Instantiate(game.Resources.LoadDynamic(instanceId));
			instance.name = "E_" + id;
			return instance.AddComponent<DynamicAvatar>();
		}

		#endregion
	}
}
