﻿using Karen90MmoFramework.Client.Data;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public struct LootItem
	{
		public short ItemId;
		public byte Index;
		public byte Count;

		public LootItem(short itemId, byte index, byte count)
		{
			this.ItemId = itemId;
			this.Index = index;
			this.Count = count;

			Status = LootItemStatus.Unlooted;
			Item = null;
		}

		/// <summary>
		/// Gets or Sets the Item
		/// </summary>
		public MmoItemData Item;

		/// <summary>
		/// Gets or Sets loot item status
		/// </summary>
		public LootItemStatus Status;
	}
}
