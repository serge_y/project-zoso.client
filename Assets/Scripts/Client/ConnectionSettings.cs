﻿namespace Karen90MmoFramework.Client
{
	public static class ConnectionSettings
	{
		/// <summary>
		/// The application name
		/// </summary>
		public static readonly string Application = "Master";

		/// <summary>
		/// The server address
		/// </summary>
		public static readonly string ServerAddress = "127.0.0.1:5055";
	}
}