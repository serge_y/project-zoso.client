﻿using System.IO;

	public class MapSettings
	{
		private string segmentName;

		private int _length;
		private int _width;

		private int _xMin, _zMin;
		private int _xMax, _zMax;

		public string SegmentName
		{
			get
			{
				return this.segmentName;
			}
		}

		public int length
		{
			get
			{
				return this._length;
			}
		}

		public int width
		{
			get
			{
				return this._width;
			}
		}

		public int xMin
		{
			get
			{
				return this._xMin;
			}
		}

		public int xMax
		{
			get
			{
				return this._xMax;
			}
		}

		public int zMin
		{
			get
			{
				return this._zMin;
			}
		}

		public int zMax
		{
			get
			{
				return this._zMax;
			}
		}

		public MapSettings(string data)
		{
			using (var reader = new StringReader(data))
			{
				//data = data.Replace(" ", string.Empty).ToLower();
				
				do
				{
					var line = reader.ReadLine().Replace(" ", string.Empty).ToLower();
					var split = line.Split('=');
					if (split.Length < 1)
						throw new FileLoadException("map data is corrupted");
					
					var first = split[0];
					var second = split[1];
					
					switch (first)
					{
						case "name":
							{
								if (second[0] == '\"')
								{
									this.segmentName = second.Substring(1, second.Length - 2);
									if (string.IsNullOrEmpty(segmentName))
										throw new FileLoadException("map data is corrupted");
								}
							}
							break;

						case "length":
							{
								if (int.TryParse(second, out _length) == false)
									throw new FileLoadException("map data is corrupted");
							}
							break;

						case "width":
							{
								if (int.TryParse(second, out _width) == false)
									throw new FileLoadException("map data is corrupted");
							}
							break;

						case "xmin":
							{
								if (int.TryParse(second, out _xMin) == false)
									throw new FileLoadException("map data is corrupted");
							}
							break;

						case "zmin":
							{
								if (int.TryParse(second, out _zMin) == false)
									throw new FileLoadException("map data is corrupted");
							}
							break;

						case "xmax":
							{
								if (int.TryParse(second, out _xMax) == false)
									throw new FileLoadException("map data is corrupted");
							}
							break;

						case "zmax":
							{
								if (int.TryParse(second, out _zMax) == false)
									throw new FileLoadException("map data is corrupted");
							}
							break;
					}
				}
				while (reader.Peek() != -1);
			}
		}
	}
